<?php
include('../connection/connect.php');

// Check if the ID parameter is set in the GET data
if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];

    // Prepare and execute the delete query using prepared statements
    $query = "DELETE FROM `tbl_contact` WHERE `id` = ?";
    $stmt = $con->prepare($query);
    $stmt->bind_param('i', $id);
    if ($stmt->execute()) {
        header('Location: read.php');
        exit;
    } else {
        echo "Error deleting data: " . $stmt->error;
    }
    $stmt->close();
} else {
    // If ID parameter is missing or empty, redirect to read.php
    header('Location: read.php');
    exit;
}
?>

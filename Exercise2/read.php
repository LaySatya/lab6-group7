<?php
include("../connection/connect.php");
include("../Header/Header.php");
?>
</head>

<body>
    <div class="container mt-5">
        <a href="create.php" class="btn btn-outline-primary">Insert</a>
    </div>
    <div class="container mt-3">
        <table id="example" class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Phone Number</th>
                    <th>Facebook</th>
                    <th>Profile</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT * FROM `tbl_contact`";
                $exe = $con->query($sql);
                if ($exe->num_rows > 0) {
                    while ($data = $exe->fetch_assoc()) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $data['id']; ?>
                            </td>
                            <td>
                                <?php echo $data['firstname']; ?>
                            </td>
                            <td>
                                <?php echo $data['lastname']; ?>
                            </td>
                            <td>
                                <?php echo $data['phone_number']; ?>
                            </td>
                            <td>
                                <?php echo $data['url_fb_social']; ?>
                            </td>
                            <td>
                                <?php
                                if ($data['profile_picture']) {
                                    echo "<div style='width: 90px; height: 90px; overflow: hidden;'>";
                                    echo "<img src='{$data['profile_picture']}' alt='no' style='width: 100%; height: 100%; object-fit: cover;'>";
                                    echo "</div>";
                                } else {
                                    echo "No Image";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="update.php?id=<?= $data['id']; ?>" class="btn btn-info">Update</a>
                                <a href="delete.php?id=<?= $data['id']; ?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>

    </div>
</body>
<?php
include("../Table/DataTable.php")
    ?>

</html>
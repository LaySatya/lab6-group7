<?php
include('../connection/connect.php');
include('../Header/Header.php');

// Check if the ID parameter is set in the GET data
if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Validate and sanitize input data
        $first_name = htmlspecialchars(trim($_POST['firstname']));
        $last_name = htmlspecialchars(trim($_POST['lastname']));
        $phone_number = htmlspecialchars(trim($_POST['phone']));
        $url_fb_social = htmlspecialchars(trim($_POST['fb_url']));

        $existing_profile_image = $_POST['current_profile']; 

        // Check if a new file is uploaded
        if (isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {
            $allowed_types = array('image/jpeg', 'image/png');
            $max_size = 5 * 1024 * 1024; 
            if (in_array($_FILES['file']['type'], $allowed_types) && $_FILES['file']['size'] <= $max_size) {
                $file_tmp = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $file_destination = '../uploadedFiles/' . $file_name;

                // Move uploaded file to destination
                if (move_uploaded_file($file_tmp, $file_destination)) {
                    $profile_image = $file_destination;
                } else {
                    echo "Error moving uploaded file.";
                    exit;
                }
            } else {
                echo "Invalid file format or size.";
                exit;
            }
        } else {
            // If no new file uploaded, maintain the existing profile image path
            $profile_image = $existing_profile_image;
        }

        // Prepare and execute the update query using prepared statements
        $query = "UPDATE `tbl_contact` 
                  SET `firstname` = ?, 
                      `lastname` = ?, 
                      `phone_number` = ?, 
                      `url_fb_social` = ?, 
                      `profile_picture` = ? 
                  WHERE `id` = ?";
        $stmt = $con->prepare($query);
        $stmt->bind_param('sssssi', $first_name, $last_name, $phone_number, $url_fb_social, $profile_image, $id);
        if ($stmt->execute()) {
            header('Location: read.php');
            exit;
        } else {
            echo "Error updating data: " . $stmt->error;
        }
        $stmt->close();
    }
} else {
    echo "ID is missing or empty.";
}
?>


<style>
    .card {
        width: 500px;
        height: inherit;
        background: #F4F6FB;
        border: 1px solid white;
        box-shadow: 10px 10px 64px 0px rgba(180, 180, 207, 0.75);
        -webkit-box-shadow: 10px 10px 64px 0px rgba(186, 186, 202, 0.75);
        -moz-box-shadow: 10px 10px 64px 0px rgba(208, 208, 231, 0.75);
    }

    .container {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        display: grid;
        justify-items: center;

    }

    .form {
        padding: 25px;
    }

    .card_header {
        display: flex;
        align-items: center;
    }

    .card svg {
        color: #7878bd;
        margin-bottom: 20px;
        margin-right: 5px;
    }

    .form_heading {
        padding-bottom: 20px;
        font-size: 21px;
        color: #7878bd;
    }

    .field {
        padding-bottom: 10px;
    }

    .input {
        border-radius: 5px;
        background-color: #e9e9f7;
        padding: 5px;
        width: 100%;
        color: #7a7ab3;
        border: 1px solid #dadaf7
    }

    .input:focus-visible {
        outline: 1px solid #aeaed6;
    }

    .input::placeholder {
        color: #bcbcdf;
    }

    label {
        color: #B2BAC8;
        font-size: 14px;
        display: block;
        padding-bottom: 4px;
    }

    button {
        background-color: #7878bd;
        margin-top: 10px;
        font-size: 14px;
        padding: 7px 12px;
        height: auto;
        font-weight: 500;
        color: white;
        border: none;
    }

    button:hover {
        background-color: #5f5f9c;
    }
</style>

<body>
    <div class="container">
        <form class="form card" action="update.php?id=<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>"
            method="post" enctype="multipart/form-data">
            <div class="card_header">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0z"></path>
                    <path fill="currentColor"
                        d="M4 15h2v5h12V4H6v5H4V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v18a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-6zm6-4V8l5 4-5 4v-3H2v-2h8z">
                    </path>
                </svg>
                <h1 class="form_heading">Update</h1>
            </div>
            <div class="field">
                <label for="firstname">Firstname</label>
                <input class="input" name="firstname" type="text" placeholder="firstname" id="firstname" required>
            </div>
            <div class="field">
                <label for="lastname">Lastname</label>
                <input class="input" name="lastname" type="text" placeholder="lastname" id="lastname" required>
            </div>
            <div class="field">
                <label for="phone">Phone number</label>
                <input class="input" name="phone" type="text" placeholder="Phone number" id="phone" required>
            </div>
            <div class="field">
                <label for="fb_url">URL_FB_SOCIAL</label>
                <input class="input" name="fb_url" type="text" placeholder="Facebook link" id="fb_url" required>
            </div>
            <div class="field">
                <label for="file">File</label>
                <input class="input" name="file[]" type="file" placeholder="file" id="file" multipart required>
            </div>
            <div class="field">
                <button class="button" type="submit" name="submit" role="submit">Update contact</button>
            </div>
            <input type="hidden" name="id" value="<?php echo isset($id) ? htmlspecialchars($id) : ''; ?>">
        </form>
    </div>
</body>

</html>
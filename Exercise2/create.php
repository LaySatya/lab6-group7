<?php
include('../connection/connect.php');
include('../Header/Header.php');
?>
    <?php
    if (isset($_POST['submit'])) {
        $file = $_FILES['profile'];
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $phoneNumber = $_POST['phoneNumber'];
        $socialLink = $_POST['social-link'];


        // handle file uploading of the image 
        $imgUrl = '';
        if ($file['error'][0] == UPLOAD_ERR_OK) {
            $imgUrl = '../uploadedFiles/' . basename($file['name'][0]);
            move_uploaded_file($file['tmp_name'][0], $imgUrl);
        }

        // now i will send all info indo database 
        $sql = "INSERT INTO tbl_contact(phone_number,firstname,lastname,url_fb_social,profile_picture) values(?,?,?,?,?)";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("issss", $phoneNumber,$firstName,$lastName,$socialLink,$imgUrl);
        $status = $stmt->execute();

        if ($status) {
            header("location: read.php");
        } else {
            echo "<p>Error: " . $stmt->error . "</p>";
        }
    }
    ?>
        <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <div class="container mt-5">
        <div class="connection-container">
        </div>

        <div class="registerUser-form">
            <h2>Insert Contact</h2>
            <form action="#" method="POST" role="form" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="firstName" class="form-label">First Name</label>
                    <input type="text" class="form-control" name="firstName" placeholder="Enter your firstname" required>
                </div>
                <div class="mb-3">
                    <label for="lastName" class="form-label">Last Name</label>
                    <input type="text" class="form-control" name="lastName" placeholder="Enter your lastname" required>
                </div>
                <div class="mb-3">
                    <label for="phoneNumber" class="form-label">Phone Number</label>
                    <input type="number" class="form-control" name="phoneNumber" placeholder="Enter your phone number" required>
                </div>
                <div class="mb-3">
                    <label for="social-link" class="form-label">Social Media Link</label>
                    <input type="text" class="form-control" name="social-link" placeholder="Enter your facebook link" required>
                </div>
                <div class="mb-3">
                    <label for="profile" class="form-label">Profile</label>
                    <input type="file" class="form-control" name="profile[]" multiple>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Insert</button>
            </form>
        </div>
    </div>
</body>

</html>